const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');

const baseURL = "https://olympics-statistics.com";

async function getSports(url) {
    try {
        const { data } = await axios.get(url);
        const $ = cheerio.load(data);
        const sports = [];

        $('a.card.sport.visible').each((index, element) => {
            const sportName = $(element).find('.bez').text().trim();
            const sportUrl = new URL($(element).attr('href'), baseURL).href;
            sports.push({ sport_name: sportName, url: sportUrl });
        });

        return sports;
    } catch (error) {
        console.error('Erreur', error);
        return [];
    }
}

async function getMedalDetails(sports) {
    const formattedSports = [];

    for (const sport of sports) {
        try {
            const response = await axios.get(sport.url);
            const $ = cheerio.load(response.data);
            const totalMedals = parseInt($('.teaser > div:first-child > span').text().trim(), 10);
            
            const disciplineMedals = {
                gold: 0,
                silver: 0,
                bronze: 0
            };

            $('.teaser > div').slice(1).each((index, element) => {
                const medalCount = parseInt($(element).find('span').text().trim(), 10);
                const medalType = parseInt($(element).find('.the-medal').attr('data-medal'), 10);

                if (medalType === 1) {
                    disciplineMedals.gold += medalCount;
                } else if (medalType === 2) {
                    disciplineMedals.silver += medalCount;
                } else if (medalType === 3) {
                    disciplineMedals.bronze += medalCount;
                }
            });

            formattedSports.push({
                sport_name: sport.sport_name,
                total_medals: totalMedals,
                medals_by_discipline: disciplineMedals
            });
        } catch (error) {
            console.error(`Erreur ${sport.url}:`, error);
        }
    }

    return formattedSports;
}


async function main() {
    const url = "https://olympics-statistics.com/olympic-sports";
    const sports = await getSports(url);
    const formattedSports = await getMedalDetails(sports);

    // JSON
    const jsonData = JSON.stringify(formattedSports, null, 2);
    fs.writeFileSync('sportsData.json', jsonData);
}

main();
