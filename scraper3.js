const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');

const baseUrl = 'https://olympics-statistics.com';
const url = baseUrl + '/olympic-athletes';
const athletesData = [];

axios.get(url)
    .then(async response => {
        const $ = cheerio.load(response.data);
        const athletes = $('a.card.athlet.visible')
        
        for (let element of athletes) {
            const firstName = $(element).find('.vn').text().trim();
            const lastName = $(element).find('.nn .n').text().trim();
            const athleteUrl = baseUrl + $(element).attr('href');

            try {
                const athleteResponse = await axios.get(athleteUrl);
                const athletePage = cheerio.load(athleteResponse.data);
                const medals = athletePage('a.m-event').map((i, el) => {
                    return {
                        eventName: $(el).find('.m-eventname').text().trim(),
                        eventDate: $(el).find('.m-event-am').text().trim(),
                        eventLocation: $(el).find('.m-event-stadt').text().trim()
                    };
                }).get();
                const athleteInfo = {
                    firstName: firstName,
                    lastName: lastName,
                    medals: medals,
                };
                athletesData.push(athleteInfo);
            } catch (athleteError) {
                console.log('Erreur:', athleteError);
            }
        }

        fs.writeFile('athletesData.json', JSON.stringify(athletesData, null, 2), () => {});
    })
    .catch(error => {
        console.log('Erreur:', error);
    });
