const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs'); 

const baseURL = "https://olympics-statistics.com/eternal-medal-table-of-the-nations";

async function scrapeData(baseURL) {
    try {
        const { data } = await axios.get(baseURL);
        const $ = cheerio.load(data);
        const countryMedals = $('.medals');
        const jsonData = [];

        countryMedals.each((index, element) => {
            const countryName = $(element).prev().text().trim();
            const countryData = {
                country: countryName,
                medals: []
            };

            $(element).find('.the-medal').each((idx, medal) => {
                const medalType = $(medal).attr('data-medal');
                const medalCount = $(medal).parent().text().trim();
                const types = { '1': 'Gold', '2': 'Silver', '3': 'Bronze' };
                const typeText = types[medalType];

                countryData.medals.push({ type: typeText, count: medalCount });
            });

            jsonData.push(countryData);
        });

        // JSON
        const jsonString = JSON.stringify(jsonData, null, 2);
        fs.writeFileSync('medalData.json', jsonString);
        return jsonData;
        
    } catch (error) {
        console.error('Erreur', error);
        return [];
    }
}


scrapeData(baseURL);
